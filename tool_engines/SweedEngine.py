from datetime import datetime
import os
import matplotlib.pyplot as plt
from tqdm import tqdm


class SweedEngine:
    extension = "out"

    def __init__(self):
        self.execution_time = None

    def execute(self, input_file: str, length: int, grid: int, removed_sequences: int, dir_name: str = None):
        print("Running SweeD...")
        start_time = datetime.now()
        os.system(f"./SweeD -name {self.extension} -input {input_file} -length {length} -grid {grid}")
        end_time = datetime.now()
        self.execution_time = str(end_time-start_time)

        if dir_name:
            run_name = "sw-" + dir_name
        else:
            run_name = "sw-" + datetime.now().strftime("%d%m%Y-%H%M%S")

        output_path = f"./output/{run_name}"
        # create new directory
        os.system(f"mkdir {output_path}")
        # move the generated files to the new directory
        os.system(f"mv *.{self.extension} {output_path}")
        print(f"SweeD execution complete; Exported to {output_path}...")
        report = output_path + "/SweeD_Report." + self.extension
        self.parse_results(report_path=report, fig_output=output_path, summary_path=output_path, plot_histogram=True,
                           removed_sequences=removed_sequences)

    def parse_results(self, report_path: str, summary_path: str, fig_output: str, removed_sequences: int,
                      plot_histogram: bool = False):
        blocks = []
        # Load the data into memory
        with open(report_path, "r") as data:
            block = []
            for x in data.readlines()[1:]:
                if x[0:2] == "//":
                    if len(block) > 0:
                        blocks.append(block)
                        block = []
                    continue
                if x.strip() and not x[:8] == "Position":
                    if x[:8] == "Position":
                        block.append(x.strip().split("\t"))
                    else:
                        block.append([float(y) for y in x.strip().split("\t")])
            blocks.append(block)

        for bl in tqdm(range(len(blocks)), desc="Creating Histograms...", total=len(blocks)):
            b = blocks[bl]
            b.sort(key=lambda z: z[1], reverse=True)
            if plot_histogram:
                self.create_histogram(f"Population {bl}; Frequency Histogram", b=b, output_path=f"{fig_output}/pop{bl}-"
                                                                                                f"histogram.jpg")
        # create new output file with the highest scores
        SUMMARY_CUTOFF = 1
        with open(summary_path + "/summary_report.txt", "w") as f:
            c = 0
            f.write(f"Execution Time: {self.execution_time}\n")
            f.write(f"Removed Sequences: {removed_sequences}\n")
            f.write(f"Number of populations: {len(blocks)}\n\n//\n")
            f.write("Position\tLikelihood\n")
            for block in blocks:
                # f.write(f"// population {c}\n")
                # f.write("Position\tLikelihood\tAlpha\tStartPos\tEndPos\n")
                # f.write("Position\tLikelihood\tAlpha\n")
                for line in block[0:SUMMARY_CUTOFF]:
                    # str_line = [str(x) for x in line]
                    # print(line)
                    if len(line) == 5:
                        # str_line = [f"{line[0]:.4f}", f"{line[1]:.6f}", f"{line[2]:.6f}", f"{line[3]:.4f}", f"{line[4]:.4f}"]
                        # str_line = [f"{line[0]:.4f}", f"{line[1]:.6f}", f"{line[2]:.6f}"]
                        str_line = [f"{line[0]:.8f}", f"{line[1]:.6f}"]
                        f.write("\t".join(str_line) + "\n")
                    else:
                        print(f"ERROR: {line}")
                # f.write("\n\n")
                c += 1

    def create_histogram(self, histogram_title: str, b: list, output_path: str):
        plt.hist([x[1] for x in b], bins=100)
        plt.gca().set(title=histogram_title)
        plt.savefig(output_path)
        plt.clf()


if __name__ == "__main__":
    x = SweedEngine()
    x.parse_results("../output/13062022-1344/SweeD_Report.out", fig_output="../output/13062022-1344",
                    summary_path="../output/13062022-1344", plot_histogram=True)
