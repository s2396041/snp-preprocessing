from abc import ABC, abstractmethod


class Filter(ABC):
    def __init__(self, data_format: str, name: str):
        self.data_format = data_format
        self.name = name

    @abstractmethod
    def filter(self, data: dict) -> None:
        """Filters the data and returns the filtered data"""
        pass

