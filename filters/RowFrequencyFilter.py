from abc import ABC
from filters.Filter import Filter

RELATIVE_FILTER_THRESHOLD = 0.05


class RowFrequencyFilterTop(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "RowFrequencyFilterTop")

    def filter(self, data: dict) -> None:
        placeholder = data.copy()
        for p in placeholder:
            no_rows_before = len(placeholder[p]['data'])
            placeholder[p]['data'].sort(key=lambda x: sum(x), reverse=True)
            no_items_to_filter = int(round(RELATIVE_FILTER_THRESHOLD * len(placeholder[p]['data'])))
            data[p]['data'] = placeholder[p]['data'][no_items_to_filter:]
            data[p]['removed_sequences'] = no_rows_before - len(data[p]['data'])


class RowFrequencyFilterBottom(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "RowFrequencyFilterBottom")

    def filter(self, data: dict) -> None:
        placeholder = data.copy()
        for p in placeholder:
            no_rows_before = len(placeholder[p]['data'])
            placeholder[p]['data'].sort(key=lambda x: sum(x), reverse=False)
            no_items_to_filter = int(round(RELATIVE_FILTER_THRESHOLD * len(placeholder[p]['data'])))
            data[p]['data'] = placeholder[p]['data'][no_items_to_filter:]
            data[p]['removed_sequences'] = no_rows_before - len(data[p]['data'])
