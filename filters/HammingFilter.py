from abc import ABC
from datetime import datetime
from tqdm import tqdm

from filters.Filter import Filter
import numpy as np
from itertools import combinations
import random


def hamming_distance(a, b):
    if not len(a) == len(b):
        return -1
    return sum(np.abs(a - b))


class Cluster:
    def __init__(self, genotype: np.array):
        self.genotypes = np.array([genotype])

    def add_genotype(self, genotype):
        self.genotypes = np.append(self.genotypes, genotype, axis=0)

    def distance(self, cl):
        total = 0
        for genotype in self.genotypes:
            for genotype2 in cl.genotypes:
                total += hamming_distance(genotype, genotype2)
        return total / (len(self.genotypes) * len(cl.genotypes))

    def merge(self, merge_type: str) -> None:
        type_before = type(self.genotypes)
        shape_before = self.genotypes.shape
        fr = np.sum(self.genotypes, axis=0)
        if merge_type == "frequency":
            self.genotypes = np.array([[int(x >= (len(self.genotypes) / 2)) for x in fr]])
        elif merge_type == "and_operation":
            self.genotypes = np.array([[int(x == len(self.genotypes)) for x in fr]])
        elif merge_type == "or_operation":
            self.genotypes = np.array([[int(x > 0) for x in fr]])
        print(f"{type_before} | {type(self.genotypes)}")
        print(f"{shape_before} | {self.genotypes.shape}")

    def append(self, cl):
        # hier bij het appenden gaat wat mis
        for gen in cl.genotypes:
            self.add_genotype([gen])

    def export(self) -> list:
        if len(self.genotypes) > 1:
            raise ValueError("You cannot export when the cluster is bigger than 1")
        return list(self.genotypes[0])

    def __str__(self):
        return str(self.genotypes)
        # return "\n".join([str(x) for x in self.genotypes])


def print_clusters(clusters):
    print("\n")
    for cluster in range(len(clusters)):
        print(f"Cluster-{cluster}:\n{str(clusters[cluster])}")
    print("\n")


def cluster(data: dict, relative_hamming_threshold=0.05, pr=False) -> list:
    result = []
    placeholder = data.copy()
    population_number = 1
    merged_populations = 0
    for p in tqdm(placeholder, desc="Merging Populations", total=len(placeholder)):
        population = placeholder[p]
        d = population['data']
        MAX_HAMMING_DISTANCE = relative_hamming_threshold * population["segsites"]
        if pr:
            print(f"Max hamming distance is {MAX_HAMMING_DISTANCE}")
        # create objects out of the data lists
        clusters = [Cluster(np.array(genotype)) for genotype in d]
        no_clusters_before = len(clusters)
        if pr:
            print(f"Started filtering with {no_clusters_before} clusters...")
            print_clusters(clusters)
        while len(clusters) > 1:
            # make a dictionary with the score as key and the clusters as value
            scores = {}
            for combination in list(combinations(clusters, 2)):
                hd = combination[0].distance(combination[1])
                scores.setdefault(hd, []).append(combination)
            min_score = min(scores.keys())
            min_values = scores[min_score]
            # check if the minimum hamming value is less than the parameter set
            if min_score > MAX_HAMMING_DISTANCE:
                break
            if len(min_values) > 1:
                # pick a random cluster and merge them
                cluster1, cluster2 = random.choice(min_values)
            else:
                # merge the two clusters with the least distance
                cluster1, cluster2 = min_values[0]

            if cluster1.genotypes.shape[1] == cluster2.genotypes.shape[1]:
                cluster1.append(cluster2)
                clusters.remove(cluster2)
            elif cluster1.genotypes.shape[1] != population['segsites']:
                print(f"Removing Faulty Line: {cluster1.genotypes.shape[1]}")
                clusters.remove(cluster1)
            elif cluster2.genotypes.shape[1] != population['segsites']:
                print(f"Removing Faulty Line: {cluster2.genotypes.shape[1]}")
                clusters.remove(cluster2)
            else:
                print("ERROR: That should not have happened")
                print(f"{cluster1.genotypes.shape} == {cluster2.genotypes.shape}")

            if pr:
                print(f"min_score: {min_score} min_values {len(min_values)}")
                print_clusters(clusters)
        no_clusters_after = len(clusters)
        # print(f"Population {population_number}; Merged {no_clusters_before - no_clusters_after} clusters; "
        #       f"Started with {no_clusters_before} clusters; Ended with {no_clusters_after} clusters")
        population_number += 1
        merged_populations += (no_clusters_after - no_clusters_before)
        result.append(clusters)
    return result


class HammingFilterFrequency(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "HammingFilterFrequency")

    def filter(self, data: dict) -> None:
        clustered_data = cluster(data)
        for population in clustered_data:
            for c in population:
                if len(c.genotypes) > 1:
                    c.merge("frequency")
        for p in data:
            sequences_before = len(data[p]['data'])
            data[p]['data'] = [cl.export() for cl in clustered_data[p]]
            data[p]['removed_sequences'] = sequences_before - len(data[p]['data'])


class HammingFilterAndOperation(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "HammingFilterAndOperation")

    def filter(self, data: dict) -> None:
        clustered_data = cluster(data)
        for population in clustered_data:
            for c in population:
                if len(c.genotypes) > 1:
                    c.merge("and_operation")
        for p in data:
            sequences_before = len(data[p]['data'])
            data[p]['data'] = [cl.export() for cl in clustered_data[p]]
            data[p]['removed_sequences'] = sequences_before - len(data[p]['data'])


class HammingFilterOrOperation(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "HammingFilterOrOperation")

    def filter(self, data: dict) -> None:
        clustered_data = cluster(data)
        for population in clustered_data:
            for c in population:
                if len(c.genotypes) > 1:
                    c.merge("or_operation")
        for p in data:
            sequences_before = len(data[p]['data'])
            data[p]['data'] = [cl.export() for cl in clustered_data[p]]
            data[p]['removed_sequences'] = sequences_before - len(data[p]['data'])
