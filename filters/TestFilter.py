from abc import ABC
from filters.Filter import Filter


class TestFilter(Filter, ABC):
    def __init__(self):
        super().__init__("ms", "TestFilter")

    def filter(self, data: dict) -> None:
        data2 = data.copy()
        for x in data2:
            temp = data2[x]
            data_data = temp['data'][1:]
            data[x]['data'] = data_data
