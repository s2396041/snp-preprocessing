from abc import ABC

from generators.Generator import Generator
import os


class MsGenerator(Generator, ABC):
    def __init__(self, output_file, generator_command):
        super().__init__(output_file, "ms")
        with open(generator_command, "r") as f:
            self.command = f.readline().strip()

    def run(self):
        os.system(f"./ms {self.command} > {self.output_file}")
