from abc import ABC

from generators.Generator import Generator
import os


class MsselGenerator(Generator, ABC):
    def __init__(self, output_file, generator_command):
        super().__init__(output_file, "mssel")
        with open(generator_command, "r") as f:
            self.command = f.readline().strip()

    def run(self):
        command = f"./mssel {self.command} > {self.output_file}"
        os.system(command)
