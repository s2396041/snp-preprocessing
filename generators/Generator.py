from abc import ABC, abstractmethod


class Generator(ABC):
    def __init__(self, output_file: str, data_format: str):
        self.output_file = output_file
        self.data_format = data_format

    @abstractmethod
    def run(self) -> None:
        """Generates the sample data and saves it locally"""
        pass

