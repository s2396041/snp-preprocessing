from parsers.MsParser import MsParser


class MsselParser(MsParser):
    def __init__(self, file_path):
        super().__init__(file_path, data_format="mssel")
