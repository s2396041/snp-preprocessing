from abc import ABC, abstractmethod


class Parser(ABC):
    def __init__(self, file_path, data_format):
        self.file_path = file_path
        self.data_format = data_format

    @abstractmethod
    def load_data(self):
        """Loads the data into python"""
        pass

    @abstractmethod
    def data_summary(self) -> str:
        """Provides a summary of the data"""
        pass
