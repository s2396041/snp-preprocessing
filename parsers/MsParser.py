from parsers.Parser import Parser


class MsParser(Parser):
    def __init__(self, file_path, data_format="ms"):
        super().__init__(file_path, data_format)
        self.command = None
        self.random_seed = None
        self.parsed_data = None
        self.load_data()

    def load_data(self) -> None:
        with open(self.file_path, "r") as f:
            self.command = f.readline().strip()
            self.random_seed = f.readline().strip()
            x = [y.strip() for y in f.readlines() if y.strip()]
            self.split_data(x)

    def split_data(self, data: list) -> None:
        result = []
        # split data into blocks
        blocks = []
        block = []
        for x in data[1:]:
            if x == "//":
                blocks.append(block)
                block = []
                continue
            block.append(x)
        blocks.append(block)
        for b in blocks:
            data = [[int(y) for y in x] for x in b[2:]]
            block_total = {
                "segsites": int(b[0].split(": ")[1]),
                "positions": [float(x,) for x in b[1].split()[1:]],
                "data": data
            }
            result.append(block_total)
        self.set_parsed_data({v: k for v, k in enumerate(result)})

    def get_parsed_data(self):
        return self.parsed_data

    def set_parsed_data(self, parsed_data):
        self.parsed_data = parsed_data

    def export(self, file_path):
        with open(file_path, "w") as f:
            f.write(f"{self.command} \n")
            f.write(f"{self.random_seed}\n\n")
            for x in self.parsed_data:
                item = self.parsed_data[x]
                f.write("//\n")
                f.write(f"segsites: {item['segsites']}\n")
                f.write(f"positions: {' '.join([format(x, '.8f') for x in item['positions']])} \n")
                for line in item['data']:
                    f.write(f"{''.join([str(x) for x in line])}\n")
                if (x+1) != len(self.parsed_data):
                    f.write("\n")

    def __str__(self):
        result = "--- Data Summary ---\n"
        return result + self.data_summary()

    def data_summary(self) -> str:
        result = f"Data format: {self.data_format}\n" \
                 f"Command: {self.command}\n" \
                 f"Random seed: {self.random_seed}\n" \
                 f"No. Populations: {len(self.parsed_data)}\n"
        return result
