import sys
import traceback


def read_summary_file(dir_name: str):
    file_name = dir_name + "/summary_report.txt"
    result = dict()
    with open(file_name, "r") as f:
        lines = f.readlines()
        result["execution_time"] = lines[0].split()[2].strip()
        result["removed_sequences"] = int(lines[1].split()[2].strip())
        result["number_of_populations"] = lines[2].split()[3].strip()
        scores = [[float(a[0]), float(a[1])] for a in [line.split() for line in lines[6:]]]
        scores.sort(key=lambda element: element[1], reverse=True)
        result["scores"] = scores
    return result


def calculate_distance(theoretical_position, report):
    scores = report.get("scores")
    if not scores:
        return -1
    result = 0.0
    for score in scores:
        result += abs(theoretical_position - score[0])
    return round(result / len(scores), 8)


class Metrics:
    """
        Example:
            -base_dir output/base_dir -unfiltered_dir output/unfiltered_dir -filtered_dir output/filtered_dir
        System args:
            -base_dir           -   str     -   The directory that contains the run for the BASE command
            -unfiltered_dir     -   str     -   The directory that contains the run for the unfiltered dataset
            -filtered_dir       -   str     -   The directory that contains the run for the filtered dataset
            -report_name        -   str     -   The name of the report that will be generated
        (optional):
            -fpr                -   double  -   The false positive rate
            -length             -   integer -   The length of the DNA sequence -> for finding theoretical position
    """

    mandatory_args = 3
    min_args = mandatory_args * 2 + 1
    args = None

    def __init__(self):
        self.args = {a: b for a, b in zip(sys.argv[1::2], sys.argv[2::2])}

        self.base_dir = self.args.get("-base_dir")
        self.unfiltered_dir = self.args.get("-unfiltered_dir")
        self.filtered_dir = self.args.get("-filtered_dir")
        self.report_name = self.args.get("-report_name")
        self.fpr = self.args.get("-fpr")
        self.length = self.args.get("-length")

        if "." not in self.report_name:
            self.report_name += ".txt"

        # checking the required parameters
        required = [
            self.base_dir,
            self.unfiltered_dir,
            self.filtered_dir,
            self.report_name
        ]
        required_names = ["-base_dir", "-unfiltered_dir", "-filtered_dir", "-report_name"]
        for requirement in range(len(required)):
            if not required[requirement]:
                raise ValueError(f"Missing parameter '{required_names[requirement]}'; Could not execute")

        # parsing summary files
        self.parsed_base_dir = read_summary_file(self.base_dir)
        self.parsed_unfiltered_dir = read_summary_file(self.unfiltered_dir)
        self.parsed_filtered_dir = read_summary_file(self.filtered_dir)

        # setting the values for fpr and distance
        if not self.fpr:
            self.fpr = 0.05
        if not self.length:
            self.length = 100000.00

        # when using ms the theoretical position of the sweep is at the center
        self.theoretical_position = self.length / 2

        # calculate the distance metric
        self.unfiltered_distance = calculate_distance(self.theoretical_position, self.parsed_unfiltered_dir)
        self.filtered_distance = calculate_distance(self.theoretical_position, self.parsed_filtered_dir)
        print(f"Unfiltered distance: {self.unfiltered_distance} \nFiltered distance: {self.filtered_distance}")

        # calculate the tpr metric
        self.unfiltered_tpr = self.calculate_tpr(self.parsed_unfiltered_dir)
        self.filtered_tpr = self.calculate_tpr(self.parsed_filtered_dir)
        print(f"Unfiltered TPR: {self.unfiltered_tpr} \nFiltered TPR: {self.filtered_tpr}")
        self.create_report()

    def calculate_tpr(self, result_set):
        scores = self.parsed_base_dir.get("scores")
        fpr_index = int(round(len(scores) * self.fpr))
        fpr_threshold = scores[fpr_index][1]
        test_scores = result_set.get("scores")
        if not test_scores:
            return -1

        counter = 0
        for score in test_scores:
            if score[1] > fpr_threshold:
                counter += 1

        return round(counter / len(test_scores), 4)

    def create_report(self):
        execution_time_unfiltered = self.parsed_unfiltered_dir.get("execution_time")
        execution_time_filtered = self.parsed_filtered_dir.get("execution_time")
        populations_unfiltered = self.parsed_unfiltered_dir.get("number_of_populations")
        populations_filtered = self.parsed_filtered_dir.get("number_of_populations")
        with open(self.report_name, "w") as f:
            f.write("=== Test Report ===\n")
            f.write("\n// Unfiltered\n")
            f.write(f"Time: \t\t{execution_time_unfiltered}\n")
            f.write(f"Populations: \t{populations_unfiltered}\n")
            f.write(f"Removed Sequences: \t {self.parsed_unfiltered_dir.get('removed_sequences')}\n")
            f.write(f"Distance: \t{self.unfiltered_distance}\n")
            f.write(f"TPR: \t\t{self.unfiltered_tpr}\n")

            f.write("\n// Filtered\n")
            f.write(f"Time: \t\t{execution_time_filtered}\n")
            f.write(f"Populations: \t{populations_filtered}\n")
            f.write(f"Removed Sequences: \t {self.parsed_filtered_dir.get('removed_sequences')}")
            f.write(f"Distance: \t{self.filtered_distance}\n")
            f.write(f"TPR: \t\t{self.filtered_tpr}")




if __name__ == "__main__":
    try:
        x = Metrics()
    except Exception as e:
        print(f"{type(e).__name__}: {e}")
        print(traceback.format_exc())
